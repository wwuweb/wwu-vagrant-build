#!/bin/bash

# Set error handling options
set -o notify
set -o nounset
set -o pipefail
set -o errexit

# Global constants
# Store the real directory of the script, not the working directory
readonly __DIR__=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
readonly __ROOT__="$(cd "$( dirname "${__DIR__}" )" && pwd)"
readonly __SELF__="${__DIR__}/$( basename "${BASH_SOURCE[0]}" )"
readonly __BASE__="$( basename "${__SELF__}" .sh )"

# Clean up the repositories, ensure there are no untracked files or changes
clean()
{
  wait

  for artifact in $@; do
    local path="${__DIR__}/$(get_repository_path ${artifact})"

    echo "Cleaning '${artifact}'..."

    if [ -d $path ]; then
      cd $path

      (
      git reset --hard
      git clean -fd
      ) > /dev/null 2>&1

      cd $__DIR__
    fi
  done
}

# Get the directory from the full repository name
get_repository_path()
{
  local repository=$1
  local path=$(expr "${repository}" : '.*/\(.*\)')

  echo $path
}

# Clone a repository into a local directory
clone_repository()
{
  local host=$1
  local repository=$2
  local path="${__DIR__}/$(get_repository_path $repository)"

  if [ ! -d $path ]; then
    git clone "git@${host}:${repository}.git" $path
  fi
}

# Pull in the latest changes on the given branch
update_repository()
{
  local repository=$1
  local branch=$2
  local path="${__DIR__}/$(get_repository_path $repository)"

  if [ ! -d $path ]; then
    echo "Repository '${repository}' was not found."
    exit 1
  fi

  echo "Updating '${repository}'..."

  cd $path

  (
  git fetch
  git checkout $branch
  git branch --set-upstream-to=origin/${branch} $branch
  git pull
  git reset --hard
  ) > /dev/null 2>&1

  cd $__DIR__
}

# Commit changes in the given repository
commit()
{
  local buildpath=$1

  cd "${__DIR__}/${buildpath}"

  git add --all .

  if [ ! -z "$(git status --porcelain)" ]; then
    read -p "Commit message for ${buildpath}: " commit_message
    git commit -m "${commit_message}"
  else
    echo "No changes to commit for '${buildpath}'"
  fi

  cd $__DIR__
}

# Dumb copy files with rsync, without times since git will determine whether
# changes occurred
copy_files()
{
  local src=$1
  local dest=$2

  echo "Copying files from '${src}' to '${dest}'..."

  rsync \
    --recursive \
    --delete-excluded \
    --exclude-from "${__DIR__}/exclude.txt" \
    $src $dest
}

__main()
{
  # Git information
  local host='bitbucket.org'
  local branch='master'

  # Build targets (vagrant)
  local vagrant='wwuweb/wwu-vagrant'
  local vagrant_nfs='wwuweb/wwu-vagrant-nfs'

  # Dependencies
  local vagrantfile='wwuweb/wwu-vagrantfile'
  local puppet='wwuweb/wwu-puppet'
  local hiera='wwuweb/wwu-hiera'

  # List components (builds, dependencies)
  local builds="${vagrant} ${vagrant_nfs}"
  local dependencies="${vagrantfile} ${puppet} ${hiera}"

  # Clean up gracefully if errors occur durring the build
  trap "{ clean ${builds}; clean ${dependencies}; exit; }" \
    ERR EXIT SIGINT SIGTERM SIGQUIT

  # Clean up before starting the build process
  clean $builds
  clean $dependencies

  # Clone repositories
  for build in $builds; do
    clone_repository $host $build
  done

  for dependency in $dependencies; do
    clone_repository $host $dependency
  done

  # Update repositories
  for build in $builds; do
    update_repository $build $branch
  done

  for dependency in $dependencies; do
    update_repository $dependency $branch
  done

  # Copy Vagrantfile
  \cp \
    "${__DIR__}/$(get_repository_path ${vagrantfile})/Vagrantfile_amd64" \
    "${__DIR__}/$(get_repository_path ${vagrant})/Vagrantfile"

  # Copy Vagrantfile NFS
  \cp \
    "${__DIR__}/$(get_repository_path ${vagrantfile})/Vagrantfile_amd64_nfs" \
    "${__DIR__}/$(get_repository_path ${vagrant_nfs})/Vagrantfile"

  # Copy Puppetfile
  \cp \
    "${__DIR__}/$(get_repository_path ${vagrantfile})/Puppetfile" \
    "${__DIR__}/$(get_repository_path ${vagrant})/puppet/Puppetfile" \

  # Copy Puppetfile NFS
  \cp \
    "${__DIR__}/$(get_repository_path ${vagrantfile})/Puppetfile" \
    "${__DIR__}/$(get_repository_path ${vagrant_nfs})/puppet/Puppetfile" \

  # Copy the other components into each build and commit any changes
  for build in $builds; do
    local buildpath=$(get_repository_path ${build})

    copy_files \
      "$(get_repository_path ${puppet})/" \
      "${buildpath}/puppet/modules"
    copy_files \
      "$(get_repository_path ${hiera})/" \
      "${buildpath}/puppet/hiera"

    commit $buildpath
  done
}

__main $@
